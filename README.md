### Palabras Claves:

* .gitlab-ci.yml -> Archivo que contiene todas las definiciones de cómo se debe construir nuestro proyecto

* script -> Define un script de shell que se ejecutará

* before_script -> Se utiliza para definir el comando que se debe ejecutar antes de (todos) los trabajos

* image -> Define qué imagen de la ventana acoplable usar

* stage -> Define una etapa de canalización (por defecto: test)

* artifacts -> Define una lista de artefactos de construcción

* artifacts:expire_in -> Se usa para eliminar artefactos cargados después del tiempo especificado

* pipelines Un pipeline es un grupo de compilaciones que se ejecutan en etapas (lotes)
